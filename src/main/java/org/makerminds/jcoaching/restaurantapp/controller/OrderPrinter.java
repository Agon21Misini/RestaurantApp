package org.makerminds.jcoaching.restaurantapp.controller;

import java.util.ArrayList;

import org.makerminds.jcoaching.restaurantapp.controller.order.AbstractOrderCalculator;
import org.makerminds.jcoaching.restaurantapp.controller.order.OrderCalculatorGER;
import org.makerminds.jcoaching.restaurantapp.controller.order.OrderCalculatorKS;
import org.makerminds.jcoaching.restaurantapp.model.Client;
import org.makerminds.jcoaching.restaurantapp.model.Location;
import org.makerminds.jcoaching.restaurantapp.model.Restaurant;
import org.makerminds.jcoaching.restaurantapp.model.order.Order;
import org.makerminds.jcoaching.restaurantapp.model.order.OrderItem;
import org.makerminds.jcoaching.restaurantapp.model.product.Product;

public class OrderPrinter {
	public void printInvoice(Order order, Restaurant restaurant, Client client, Location location) {
		AbstractOrderCalculator orderCalculator = getOrderCalculator(location);
		double totalOrderAmount = orderCalculator.claculateTotalOrderAmount(order);
		double totalOrderAmountVAT = orderCalculator.calculateTotalOrderAmountVAT(totalOrderAmount);
		double totalOrderAmountWithVAT = totalOrderAmount + totalOrderAmountVAT;
		ArrayList<OrderItem> orderItems = order.getOrderItems();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Order from ").append(client.getName()).append(": ").append(System.lineSeparator())
				.append("----------------------------------------");
		System.out.println(stringBuilder.toString());

		// Java Stream API (Lambda Expression)
		orderItems.forEach(o -> printOrderItemInfo(o));

		/*
		 * for (OrderItem orderItem : orderItems) { Product product =
		 * orderItem.getProduct(); double totalOrderItemPrice =
		 * orderItem.getOrderItemPrice() * orderItem.getQuantity();
		 * printOrderItemInfo(orderItem, product, totalOrderItemPrice); }
		 */

		int vatRate = (int) orderCalculator.getVATRate(false);
		StringBuilder stringBoulder = new StringBuilder();
		stringBoulder.append("----------------------------------------").append(System.lineSeparator())
				.append("The total price of the order is: ").append(System.lineSeparator()).append("SUB TOTAL: ")
				.append(totalOrderAmount).append("$ ").append(System.lineSeparator()).append("VAT ").append(vatRate)
				.append("%: ").append(totalOrderAmountVAT).append("$ ").append(System.lineSeparator()).append("TOTAL: ")
				.append(totalOrderAmountWithVAT).append("$").append(System.lineSeparator())
				.append("----------------------------------------").append(System.lineSeparator()).append("Restaurant ")
				.append(restaurant.getName()).append(" in ").append(restaurant.getAddress());
		System.out.println(stringBoulder.toString());
	}

	private void printOrderItemInfo(OrderItem orderItem) {
		Product product = orderItem.getProduct();
		double totalOrderItemPrice = orderItem.getOrderItemPrice() * orderItem.getQuantity();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(orderItem.getQuantity()).append("x | ").append(product.getName()).append(" | ")
				.append(orderItem.getOrderItemPrice()).append("$ | ").append(totalOrderItemPrice).append("$");
		System.out.println(stringBuilder.toString());
	}

	private AbstractOrderCalculator getOrderCalculator(Location location) {
		switch (location) {
		case KOSOVO:
			return new OrderCalculatorKS();
		case GERMANY:
			return new OrderCalculatorGER();
		default:
			System.err.println("Current location is invalid");
			return null;
		}
	}
}