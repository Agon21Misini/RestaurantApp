package org.makerminds.jcoaching.restaurantapp.controller.menu;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.makerminds.jcoaching.restaurantapp.model.Menu;
import org.makerminds.jcoaching.restaurantapp.model.product.Drink;
import org.makerminds.jcoaching.restaurantapp.model.product.Meal;
import org.makerminds.jcoaching.restaurantapp.model.product.Product;

public class MenuImporter {
	public Menu importMenu(String filePathAsString) {
		Menu importedMenu = new Menu();
		Path filePath;
		try {
			filePath = Paths.get(getClass().getResource(filePathAsString).toURI());
			List<String> menuasStringList = Files.readAllLines(filePath);
			for (String menuItemAsString : menuasStringList) {
				String[] menuItemStringArray = menuItemAsString.split(",");
				int productId = Integer.valueOf(menuItemStringArray[0]);
				String productName = menuItemStringArray[1];
				double productPrice = Double.valueOf(menuItemStringArray[2]);
				String productCategory = menuItemStringArray[3];

				Product product = null;
				if ("meal".equals(productCategory)) {
					product = new Meal(productName, productId, productPrice);
				} else if ("drink".equals(productCategory)) {
					boolean sugarFree = Boolean.valueOf(menuItemStringArray[4]);
					product = new Drink(productName, productId, productPrice, sugarFree);
				} else {
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.append("The menu file couldn't be processed as the product category from product")
							.append(productName).append(" is invalid");
					throw new IllegalArgumentException(stringBuilder.toString());
				}
				importedMenu.getMenuItems().put(productId, product);
			}
		} catch (URISyntaxException | IOException e) {
			throw new RuntimeException("Menu file could not be found");
		}
		return importedMenu;
	}
}
