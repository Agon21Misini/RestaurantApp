package org.makerminds.jcoaching.restaurantapp.gui;

import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import org.makerminds.jcoaching.restaurantapp.controller.menu.MenuImporter;
import org.makerminds.jcoaching.restaurantapp.model.Menu;
import org.makerminds.jcoaching.restaurantapp.model.product.Product;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

public class RestaurantAppGUI {

	private JFrame frame;
	private final String MENU_FILE_PATH = "/pizza-menu.txt";
	private DefaultTableModel defaultTableModel = new DefaultTableModel();
	private boolean readMenuFromFile = true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RestaurantAppGUI window = new RestaurantAppGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RestaurantAppGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel applicationNameLabel = new JLabel("RestaurantApp (JCoaching)");
		applicationNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		applicationNameLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		frame.getContentPane().add(applicationNameLabel, BorderLayout.NORTH);

		prepareMenuDataTable();
		
		JTable table = new JTable(defaultTableModel);
		frame.getContentPane().add(table, BorderLayout.SOUTH);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBorder(BorderFactory.createTitledBorder("Restaurant Menu"));
//		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();		
		JButton switchMenuButton = createSwitchMenuButton();
		panel.add(switchMenuButton);
		panel.add(scrollPane);
		
		frame.getContentPane().add(panel);
	}

	private JButton createSwitchMenuButton() {
		JButton switchMenuButton = new JButton("Switch Menu");
		switchMenuButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				readMenuFromFile = readMenuFromFile ? false : true;
				prepareMenuDataTable();
			}
		});
		return switchMenuButton;
	}
	private void prepareMenuDataTable() {
		String[] columnHeader = { "Product Id", "Product Name", "Product Price" };
		Menu menu = getMenu();
		String[][] menuArray = createMenuArray(menu);
		
		defaultTableModel.setDataVector(menuArray, columnHeader);
	}

	private Menu getMenu() {
		Menu menu;
		if(readMenuFromFile) {
			MenuImporter menuImporter = new MenuImporter();
			menu = menuImporter.importMenu(MENU_FILE_PATH);
		} else {
			menu = new Menu(true);
		}
		return menu;
	}

	private String[][] createMenuArray(Menu menu) {
		HashMap<Integer, Product> menuItems = menu.getMenuItems();
		String[][] menuArray = new String[menuItems.size()][3];

		int i = 0;
		for (Entry<Integer, Product> menuItem : menuItems.entrySet()) {
			Product product = menuItem.getValue();
			menuArray[i][0] = Integer.toString(product.getProductId());
			menuArray[i][1] = product.getName();
			menuArray[i][2] = Double.toString(product.getPrice());
			i++;
		}
		return menuArray;
	}

}
