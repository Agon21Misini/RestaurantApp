package org.makerminds.jcoaching.restaurantapp.controller.menu;

import java.util.Map.Entry;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.makerminds.jcoaching.restaurantapp.model.Menu;
import org.makerminds.jcoaching.restaurantapp.model.product.Product;

public class MenuImporterTest {
	private MenuImporter menuImporter = new MenuImporter();
	private final String MENU_FILE_PATH = "/menu-test.txt";

	@Test
	public void testImportMenu() {
		Menu importMenu = menuImporter.importMenu(MENU_FILE_PATH);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("---------------MENU---------------").append(System.lineSeparator());
		for (Entry<Integer, Product> menuItemEntry : importMenu.getMenuItems().entrySet()) {
			Product menuItem = menuItemEntry.getValue();
			stringBuilder.append(menuItem.getProductId()).append(". ").append(menuItem.getName()).append(" | ")
					.append(menuItem.getPrice()).append("$").append(System.lineSeparator());
		}
		stringBuilder.append("----------------------------------").append(System.lineSeparator());
		System.out.println(stringBuilder.toString());
		Assertions.assertEquals(8, importMenu.getMenuItems().size());
	}
}
